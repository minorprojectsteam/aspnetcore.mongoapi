namespace MongoApi1

open System
open System.Collections.Generic
open System.IO
open System.Linq
open System.Threading.Tasks
open Microsoft.AspNetCore
open Microsoft.AspNetCore.Hosting
open Microsoft.Extensions.Configuration
open Microsoft.Extensions.Logging

module Program =
    let exitCode = 0

    let BuildWebHost args =
        WebHost
            .CreateDefaultBuilder(args)
            .ConfigureAppConfiguration(fun context builder -> 
                                                                let env = context.HostingEnvironment
                                                                builder
                                                                    .AddJsonFile("appsettings.json", true, false)
                                                                    .AddJsonFile((sprintf "appsettings.%s.json" (env.EnvironmentName)), false, true)
                                                                    |> ignore)
            .UseStartup<Startup>()
            .Build()

    [<EntryPoint>]
    let main args =
        BuildWebHost(args).Run()

        exitCode
