namespace MongoApi1.Configuration
open Microsoft.Extensions.Configuration
open Microsoft.Extensions.DependencyInjection
open Microsoft.AspNetCore.Cors.Infrastructure
open Microsoft.Extensions.Options;
open Microsoft.AspNetCore.Builder
open System.ComponentModel
open System
open MongoApi1.Mongo

module ServiceCollection =
    let addOptions (c: IConfiguration) (services: IServiceCollection) =
        services
            .AddOptions()
            .Configure<MongoOptions>(c.GetSection("MongoConnection"))
            .AddSingleton<MongoOptions>(fun (cfg: IServiceProvider) -> cfg.GetService<IOptions<MongoOptions>>().Value)

    let addCorsPolicies (services: IServiceCollection) =
        services
            .AddCors()

    let addCompositionRoot (services: IServiceCollection) =
        services
            .AddTransient<MongoRepo>()

    let addMvc (services: IServiceCollection) = 
        services
            .AddMvc()


module AppBuilder =
    let useWhen condition func (app : IApplicationBuilder): IApplicationBuilder =
        if condition() then func(app)
        else app
    
    let useCors (name: string) (app : IApplicationBuilder) =
        app.UseCors(name)
    
    let useWelcomePage (path: string) (app : IApplicationBuilder) =
        app.UseWelcomePage(path)

    let useMvc (app : IApplicationBuilder) =
        app.UseMvc()