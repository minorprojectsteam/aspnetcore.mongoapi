namespace MongoApi1.Configuration

type MongoOptions() =
        member val ConnectionString : string = "" with get,set
        member val Database: string = "" with get,set