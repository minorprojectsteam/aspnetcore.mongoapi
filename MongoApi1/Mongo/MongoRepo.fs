namespace MongoApi1.Mongo

open MongoApi1.Features.Notes
open FSharp.Core
open MongoApi1.Configuration
open MongoDB.Driver
open MongoDB.Driver.Core
open MongoDB.Bson
open System
open FSharp.Linq.QueryRunExtensions
open System.Linq
open System.Collections.Generic
open MongoDB.FSharp

type IMongoRepo =
    abstract member GetAll: unit -> IEnumerable<Note>
    abstract member GetOne: System.Guid -> Note
    abstract member SaveOne: Note -> unit
    abstract member UpdateOne: Note -> Note
    abstract member DeleteOne: Guid -> unit
    abstract member UpsertOne: Note -> Note

type MongoRepo(o: MongoOptions) =
    let _context: MongoContext = MongoContext(o)
    let _collection: IMongoCollection<Note> = _context.Notes

    member this.getAll () = _collection.Find(Builders.Filter.Empty).ToEnumerable()

    member this.saveOne (note: Note) = _collection.InsertOne(note)

    member this.getOne (id: Guid) =
            _collection.Find(fun x -> x.Id = id).ToEnumerable()

    member this.updateOne (note: Note) =
        let filter = 
            Builders<Note>.Filter.Eq((fun x -> x.Id), note.Id)
        let updateDef = 
            Builders<Note>.Update
                                .Set((fun (x:Note) -> x.Title), note.Title)
                                .Set((fun (x:Note) -> x.Body), note.Body)

        _collection.UpdateOne(filter, updateDef)

    member this.deleteOne (id: Guid) = _collection.DeleteOne(fun n -> n.Id = id)

