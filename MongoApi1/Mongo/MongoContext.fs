namespace MongoApi1.Mongo

open MongoDB.Driver
open MongoApi1.Configuration
open MongoApi1.Features.Notes

type MongoContext (o: MongoOptions) =
    let _client = MongoClient(o.ConnectionString)
    member val _database: IMongoDatabase = _client.GetDatabase o.Database with get

    member ctx.Notes: IMongoCollection<Note> = ctx._database.GetCollection<Note>("NotesCollection")