namespace MongoApi1

open System
open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Hosting
open Microsoft.AspNetCore.Http
open Microsoft.Extensions.DependencyInjection
open Microsoft.Extensions.Configuration
open MongoApi1.Configuration.ServiceCollection
open MongoApi1.Configuration.AppBuilder

type Startup private() =
    new (conf: IConfiguration) as x =
        Startup() then
        x.config <- conf

    member val config: IConfiguration = null with get,set

    member x.ConfigureServices(services: IServiceCollection) =
        services
        |> addCorsPolicies
        |> addOptions x.config
        |> addCompositionRoot
        |> addMvc
        |> ignore

    member x.Configure(app: IApplicationBuilder, env: IHostingEnvironment) =
        if env.IsDevelopment() 
            then app
                    .UseDeveloperExceptionPage()
                    .UseStatusCodePages()
            else app
        |> useCors "ALLOWANY"
        |> useWelcomePage "/"
        |> useMvc
        |> ignore