namespace MongoApi1.Features.Notes

open System
open MongoDB.Bson.Serialization.Attributes;

// type Note = {
//     Id: Guid;
//     Title: string;
//     Body: string;
//     CreatedOn: DateTime
// }

type Note () =
    [<BsonId>]
    member val Id: Guid = Guid.Empty with get,set
    member val Title: string = String.Empty with get,set
    member val Body: string = String.Empty with get,set
    member val CreatedOn: DateTime = DateTime.MinValue with get,set
