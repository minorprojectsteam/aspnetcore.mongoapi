namespace MongoApi1.Features.Notes

open System
open System.Collections.Generic
open System.Linq
open System.Threading.Tasks
open Microsoft.AspNetCore.Mvc
open Microsoft.Extensions.Options
open MongoApi1.Configuration
open Microsoft.Extensions.Configuration
open Microsoft.AspNetCore.Http
open MongoApi1.Mongo

[<RouteAttribute("api/[controller]")>]
[<ConsumesAttribute("application/json")>]
type NotesController (repo: MongoRepo) =
    inherit Controller()

    let _repo: MongoRepo = repo

    [<HttpGetAttribute>]
    member n.GetAll () =
           n.Ok(_repo.getAll())

    [<HttpGetAttribute("{id}")>]
    member n.GetOne (id: Guid) =
           n.Ok(_repo.getOne(id))

    [<HttpPostAttribute>]
    member n.PostOne ([<FromBodyAttribute>]note: Note) =
           n.Ok(_repo.saveOne(note))

    [<HttpPutAttribute("put")>]
    member n.PutOne ([<FromBodyAttribute>]note: Note) =
           n.Ok(_repo.updateOne(note))

    [<HttpDelete("delete/{id}")>]
    member n.DeleteOne (id: Guid) =
           n.Ok(_repo.deleteOne(id))

    [<HttpPutAttribute("upsert")>]
    member n.UpsertOne ([<FromBodyAttribute>]note: Note) =
           n.Ok(note)
