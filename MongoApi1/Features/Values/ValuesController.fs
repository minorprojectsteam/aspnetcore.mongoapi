namespace MongoApi1.Features.Values

open System
open System.Collections.Generic
open System.Linq
open System.Threading.Tasks
open Microsoft.AspNetCore.Mvc
open Microsoft.Extensions.Options
open MongoApi1.Configuration
open Microsoft.Extensions.Configuration

[<RouteAttribute("api/[controller]")>]
type ValuesController private() =
    inherit Controller()

    member val options: MongoOptions = new MongoOptions() with get,set

    new (c: MongoOptions) as v =
        new ValuesController() then
        v.options <- c

    [<HttpGetAttribute>]
    member v.Get () =
        v.Ok("Hello world")

    [<HttpGetAttribute("check")>]
    member v.Options () =
        v.options.ConnectionString.ToString()